Altium Designer Pick and Place Locations
C:\Users\ikremast\clictd_chipboard\ProductionFiles\Pick Place\Pick Place for CLICTD_chipboard_cut(SMD_Variant_B).txt

========================================================================================================================
File Design Information:

Date:       23/03/21
Time:       18:41
Revision:   87b4046bb45b90eef424c6002cc3fe549a1240bb
Variant:    SMD_Variant_B
Units used: mm

Designator Comment                        Layer       Footprint                      Center-X(mm) Center-Y(mm) Rotation Description                                                                                                   
FTG6       "Use only in PCB"              BottomLayer FIDUCIAL_TOP_S100-200          69.0000      30.0000      90       "Fiducial Target"                                                                                             
FTG5       "Use only in PCB"              BottomLayer FIDUCIAL_TOP_S100-200          69.0000      85.0000      90       "Fiducial Target"                                                                                             
FTG4       "Use only in PCB"              BottomLayer FIDUCIAL_TOP_S100-200          31.0000      85.0000      90       "Fiducial Target"                                                                                             
FTG3       "Use only in PCB"              TopLayer    FIDUCIAL_TOP_S100-200          69.0000      30.0000      90       "Fiducial Target"                                                                                             
FTG2       "Use only in PCB"              TopLayer    FIDUCIAL_TOP_S100-200          69.0000      85.0000      90       "Fiducial Target"                                                                                             
FTG1       "Use only in PCB"              TopLayer    FIDUCIAL_TOP_S100-200          31.0000      85.0000      90       "Fiducial Target"                                                                                             
R8         470                            TopLayer    RESC1608X55N                   61.2500      79.4500      180      "Resistor - 1%"                                                                                               
R7         470                            TopLayer    RESC1608X55N                   61.2500      74.2500      360      "Resistor - 1%"                                                                                               
C16        1uF                            TopLayer    CAPC3225X168N                  60.5000      76.8500      0        "Non-polarised capacitor"                                                                                     
U2         MAX868EUB+                     TopLayer    SOP50P490X110-10N              34.0000      28.0000      360      "Regulated, Adjustable -2x Inverting Charge Pump"                                                             
U1         MAX868EUB+                     TopLayer    SOP50P490X110-10N              20.0000      28.0000      360      "Regulated, Adjustable -2x Inverting Charge Pump"                                                             
R10        390k                           TopLayer    RESC1608X55N                   34.0000      30.6500      360      "Resistor - 1%"                                                                                               
R9         390k                           TopLayer    RESC1608X55N                   20.0000      30.6500      360      "Resistor - 1%"                                                                                               
R6         200k                           TopLayer    RESC1608X55N                   38.0500      29.9000      270      "Resistor - 1%"                                                                                               
R5         200k                           TopLayer    RESC1608X55N                   24.0500      30.0000      270      "Resistor - 1%"                                                                                               
C22        100nF                          TopLayer    CAPC1608X87N                   38.0500      26.7500      270      "Non-polarised capacitor"                                                                                     
C21        100nF                          TopLayer    CAPC1608X87N                   24.0500      26.8500      270      "Non-polarised capacitor"                                                                                     
C20        10uF                           TopLayer    CAPMP3528X210N                 31.8000      23.4500      180      "Polarised capacitor"                                                                                         
C19        10uF                           TopLayer    CAPMP3528X210N                 17.8000      23.4500      180      "Polarised capacitor"                                                                                         
C18        100nF                          TopLayer    CAPC1608X87N                   29.9500      26.8500      90       "Non-polarised capacitor"                                                                                     
C17        100nF                          TopLayer    CAPC1608X87N                   15.9500      26.8500      90       "Non-polarised capacitor"                                                                                     
C15        1uF                            TopLayer    CAPC1608X87N                   35.3000      23.6500      90       "Non-polarised capacitor"                                                                                     
C14        1uF                            TopLayer    CAPC1608X87N                   21.3000      23.6500      90       "Non-polarised capacitor"                                                                                     
R12        2k7                            TopLayer    RESC1608X55N                   22.5000      35.7500      360      "Resistor - 1%"                                                                                               
R11        2k7                            TopLayer    RESC1608X55N                   22.5000      37.7500      360      "Resistor - 1%"                                                                                               
R19        0                              BottomLayer RESC1608X55N                   31.0500      51.0000      360      Resistor                                                                                                      
R17        0                              BottomLayer RESC1608X55N                   31.0500      55.0000      360      Resistor                                                                                                      
R4         470                            TopLayer    RESC1608X55N                   54.2500      79.4500      180      "Resistor - 1%"                                                                                               
J2         "REVERSE BIAS"                 TopLayer    TSW-105-07-T-D                 60.2000      70.2000      360      ""                                                                                                            
C24        100nF                          TopLayer    CAPC1608X87N                   17.1000      40.2000      90       "Non-polarised capacitor"                                                                                     
C23        100nF                          TopLayer    CAPC1608X87N                   17.1000      49.8000      270      "Non-polarised capacitor"                                                                                     
C4         10nF                           TopLayer    CAPC1608X87N                   41.8000      40.5000      360      "Non-polarised capacitor"                                                                                     
R13        2k7                            TopLayer    RESC1608X55N                   15.3000      49.8000      90       "Resistor - 1%"                                                                                               
R14        2k7                            TopLayer    RESC1608X55N                   13.5000      49.8000      90       "Resistor - 1%"                                                                                               
R15        4k7                            TopLayer    RESC1608X55N                   15.3000      40.2000      270      "Resistor - 1%"                                                                                               
R16        4k7                            TopLayer    RESC1608X55N                   13.5000      40.2000      270      "Resistor - 1%"                                                                                               
U3         TCA9509DGKR                    TopLayer    DGK8_N                         15.0000      45.0000      270      "Level Translating I2C Bus Repeater, 2.7 to 5.5 V, -40 to 85 degC, 8-pin MSOP (DGK), Green (RoHS & no Sb/Br)" 
C11        10nF                           BottomLayer CAPC1608X87N                   45.3000      39.7500      360      "Non-polarised capacitor"                                                                                     
C10        10nF                           BottomLayer CAPC1608X87N                   45.3000      38.1500      360      "Non-polarised capacitor"                                                                                     
C9         10uF                           BottomLayer CAPMP3528X210N                 45.1000      32.8500      270      "Polarised capacitor"                                                                                         
C12        100nF                          BottomLayer CAPC1608X87N                   45.3000      36.5500      360      "Non-polarised capacitor"                                                                                     
C1         10uF                           TopLayer    CAPMP3528X210N                 41.6000      33.6000      270      "Polarised capacitor"                                                                                         
C2         100nF                          TopLayer    CAPC1608X87N                   41.8000      37.3000      360      "Non-polarised capacitor"                                                                                     
C3         10nF                           TopLayer    CAPC1608X87N                   41.8000      38.9000      360      "Non-polarised capacitor"                                                                                     
J1         SAMTEC_SEAM-40-03.5-S-08-2-A-K BottomLayer SAMTEC_SEAM-40-03.5-S-08-2-A-K 49.9552      14.3300      180      ""                                                                                                            
C5         10uF                           TopLayer    CAPMP3528X210N                 57.5000      34.1000      270      "Polarised capacitor"                                                                                         
C6         100nF                          TopLayer    CAPC1608X87N                   57.3000      37.8000      180      "Non-polarised capacitor"                                                                                     
C7         10nF                           TopLayer    CAPC1608X87N                   57.3000      39.4000      180      "Non-polarised capacitor"                                                                                     
C8         10nF                           TopLayer    CAPC1608X87N                   57.3000      41.0000      180      "Non-polarised capacitor"                                                                                     
C13        1uF                            TopLayer    CAPC3225X168N                  53.5000      76.8500      360      "Non-polarised capacitor"                                                                                     
R3         470                            TopLayer    RESC1608X55N                   54.2500      74.2500      360      "Resistor - 1%"                                                                                               
